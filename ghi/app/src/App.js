import { BrowserRouter, Routes, Route } from 'react-router-dom';
import MainPage from './MainPage';
import Nav from './Nav';
import ListManufacturers from './ManufacturersList';
import CreateManufacturer from './CreateManufacturer';
import AutomobileList from './AutomobileList';
import ModelsList from './ModelsList';
import VehicleModelForm from './VehiclModelForm';
import CreateAutomobile from './AutomobileCreate';
import CustomerCreate from './CustomerCreate';
import CustomersList from './CustomersList';
import SalespersonCreate from './SalespersonCreate';
import SalespersonList from './SalespersonList';
import SalesCreate from './SalesCreate';
import SalesList from './SalesList';
import SalespersonHistory from './SalespersonHistory'
import TechniciansList from './TechniciansList';
import CreateTechnician from './CreateTechnician';
import AppointmentList from './AppointmentList';
import AppointmentHistoryList from './AppointmentHistory';
import CreateAppointment from './CreateAppointment';


function App() {
  return (
    <BrowserRouter>
      <Nav />
      <div className="container">
        <Routes>
          <Route path="/" element={<MainPage />} />
          <Route path="/manufacturers" element={<ListManufacturers />} />
          <Route path="/manufacturers/create" element={<CreateManufacturer />} />
          <Route path="/automobiles" element={<AutomobileList />} />
          <Route path="/models" element={<ModelsList />} />
          <Route path="/models/create" element={<VehicleModelForm />} />
          <Route path="/automobiles/create" element={<CreateAutomobile />} />
          <Route path="/customer/create" element={<CustomerCreate />} />
          <Route path="/customers" element={<CustomersList />} />
          <Route path="/salesperson/create" element={<SalespersonCreate />} />
          <Route path="/salesperson" element={<SalespersonList />} />
          <Route path="/sales/create" element={<SalesCreate />} />
          <Route path="/sales" element={<SalesList />} />
          <Route path="/salespersonhistory" element={<SalespersonHistory />} />
          <Route path="/technicians" element={<TechniciansList />} />
          <Route path="/technicians/create" element={<CreateTechnician/>} />
          <Route path="/appointments" element={<AppointmentList />} />
          <Route path="/appointments/history" element={<AppointmentHistoryList />} />
          <Route path="/appointment/create" element={<CreateAppointment />} />
        </Routes>
      </div>
    </BrowserRouter>
  );
}

export default App;
