from django.db import models


class Technician(models.Model):
    first_name = models.CharField(max_length=200)
    last_name = models.CharField(max_length=200)
    employee_id = models.SmallIntegerField(unique=True)


class AutomobileVO(models.Model):
    import_vin = models.CharField(max_length=17)
    sold = models.BooleanField(default=False)


class Appointment(models.Model):
    date_time = models.DateTimeField()
    reason = models.CharField(max_length=200)
    vin = models.CharField(max_length=17)
    customer = models.CharField(max_length=200)
    status = models.BooleanField(default=False)

    technician = models.ForeignKey(
        AutomobileVO,
        related_name="technician",
        on_delete=models.CASCADE
    )

    # def __str__(self):
    #     return self.customer

# Create your models here.
