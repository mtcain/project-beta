import { useEffect, useState } from 'react';

const AppointmentList = () => {
  const [appointments, setAppointments] = useState([]);
  const [automobiles, setAutomobiles] = useState([]);

  const fetchData = async () => {
    const url = 'http://localhost:8080/api/appointments/';
    const response = await fetch(url);

    if (response.ok) {
      const data = await response.json();
      console.log(data);
      setAppointments(data.appointment);
    } else {
      console.log('Response not OK;', response.status, response.statusText);
    }
  }


  const fetchAutomobiles = async () => {
    const url = 'http://localhost:8080/api/automobilevo/';
    const response = await fetch(url);

    if (response.ok) {
      const data = await response.json();
      console.log(data);
      setAutomobiles(data.automobilevo);
    } else {
      console.log('Response not OK;', response.status, response.statusText);
    }
  };


  const handleCancelAppointment = (appointment) => {
    const url = `http://localhost:8080/api/appointments/${appointment.vin}/cancel`;
    handlePutRequest(url);
  };

  const handleFinishAppointment = (appointment) => {
    const url = `http://localhost:8080/api/appointments/${appointment.vin}/finish`;
    handlePutRequest(url);
  };



  const handlePutRequest = (url) => {
    fetch(url, {
      method: 'PUT',
      headers: {
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({}),
    }) .then(response => {
        if (response.ok) {
        } else {
          throw new Error('PUT request failed');
        }
      }) .catch(error => {
        console.error(error);
      });
  };

  useEffect(()=>{
    fetchData();
    fetchAutomobiles();
  }, [])

  const filteredAppointments = appointments.filter(appointment => appointment.status === 'CREATED');

  const checkVinEquality = (vin1, vin2) => {
    return vin1.toLowerCase() === vin2.toLowerCase();
  };
  return (
      <table className="table table-striped">
      <thead>
        <tr>
          <th colSpan="2" style={{ fontSize: '35px'}}>Appointments</th>
        </tr>
        <tr>
          <th>VIN</th>
          <th>Is VIP?</th>
          <th>Customer</th>
          <th>Date and Time</th>
          <th>Technician</th>
          <th>Reason</th>
          <th>Status</th>
        </tr>
      </thead>
      <tbody>
      {filteredAppointments.map((appointment) => {
          const automobile = automobiles.find((auto) =>
            checkVinEquality(appointment.vin, auto.import_vin)
          );

          const isVip = automobile && automobile.sold ? 'Yes' : 'No';
          return (
              <tr key={appointment.vin}>
                <td>{appointment.vin}</td>
                <td>{isVip}</td>
                <td>{appointment.customer}</td>
                <td>{appointment.date_time}</td>
                <td>{appointment.technician.first_name} {appointment.technician.last_name}</td>
                <td>{appointment.reason}</td>
                <td>
                    <button onClick={() => handleCancelAppointment(appointment)}>Cancel</button>
                    <button onClick={() => handleFinishAppointment(appointment)}>Finish</button>
                </td>
              </tr>
          );
        })}
      </tbody>
      </table>
  );
}

export default AppointmentList;