import React, { useEffect, useState } from "react";

const SaleForm = () => {
    const [selectedAutomobileVin, setSelectedAutomobileVin] = useState([]);
    const [automobiles, setAutomobiles] = useState([]);
    const [customers, setCustomers] = useState([]);
    const [salespeople, setSalespeople] = useState([]);
    const [formData, setFormData] = useState({

        price: '',
        automobile: {
            vin_input: ''
        },
        customer: {
            address_input: ''
        },
        salesperson: {
            employee_id_input: ''
        }
    });

    const getDataAutomobile = async() => {
        const url = 'http://localhost:8100/api/automobiles/';
        const response = await fetch(url);

        if (response.ok) {
            const data = await response.json();
            setAutomobiles(data.autos);
        };
    };

    const getDataCustomer = async() => {
        const url = 'http://localhost:8090/api/customers/';
        const response = await fetch(url);

        if (response.ok) {
            const data = await response.json();
            setCustomers(data.customer);
        };
    };

    const getDataSalespeople = async() => {
        const url = 'http://localhost:8090/api/salespeople/';
        const response = await fetch(url);

        if (response.ok) {
            const data = await response.json();
            setSalespeople(data.salesperson);
        };
    };

    useEffect(() => {
        getDataAutomobile();
        getDataCustomer();
        getDataSalespeople();
    },[]);

    const handleSubmit = async (event) => {
        event.preventDefault();

        const saleUrl = 'http://localhost:8090/api/sales/';
        const automobile_vinUrl = `http://localhost:8090/api/automobiles/${selectedAutomobileVin}`;

        const fetchConfig = {
            method:"post",
            body: JSON.stringify(formData),
            headers: {
                'Content-Type': 'application/json',
            },
        };

        const fetchConfigPut = {
            method: 'put',
            body: JSON.stringify({ sold: true }),
            headers: {
              'Content-Type': 'application/json',
            },
          };

        const response = await fetch(saleUrl, fetchConfig);
        const response_update = await fetch(automobile_vinUrl, fetchConfigPut);

        if (response.ok&&response_update.ok) {
          setFormData({
            price: '',
            automobile: {
              vin_input: '',
            },
            customer: {
              address_input: '',
            },
            salesperson: {
              employee_id_input: '',
            },
          });
        }
      };



    const handleFormChange = (e) =>{
        const value = e.target.value;
        const inputName=e.target.name;

        if (inputName === 'vin') {
            setSelectedAutomobileVin(value);
        }

        setFormData({

            ...formData,

            [inputName]: value
        }

        );
    }

    return (
        <div className="row">
            <div className="offset-3 col-6">
                <div className="shadow p-4 mt-4">
                    <h1>Record a new sale</h1>
                    <form onSubmit={handleSubmit} id="create-sale-form">
                    <div className="mb-3">
                        <select onChange={handleFormChange} value={formData.automobile.vin_input} required name="automobile" id="vin" className="form-select">
                        <option value="">Autombile Vin</option>
                        {automobiles.map((automobile) => {

                            return (
                                <option key={automobile.vin} value={automobile.vin}>{automobile.vin}</option>

                            )

                            })}
                        </select>
                    </div>
                    <div className="mb-3">
                        <select onChange={handleFormChange} value={formData.salesperson.employee_id} required name="first_name" id="" className="form-select">
                        <option value="">Salesperson</option>
                        {salespeople.map((employee) => {

                            return (
                                <option key={employee.employee_id} value={employee.employee_id}>{employee.first_name} {employee.last_name}</option>

                            )

                            })}
                        </select>
                    </div>
                    <div className="mb-3">
                        <select onChange={handleFormChange} value={formData.customer.phone_number} required name="first_name" id="" className="form-select">
                        <option value="">Customer</option>
                        {customers.map((customer) => {

                            return (
                                <option key={customer.phone_number} value={customer.phone_number}>{customer.first_name} {customer.last_name}</option>

                            )

                            })}
                        </select>
                    </div>
                    <div className="form-floating mb-3">
                        <input onChange={handleFormChange} value={formData.price} placeholder="Price" required type="number" name="price" id="price" className="form-control" />
                        <label htmlFor="name">Price</label>
                    </div>
                    <button className="btn btn-primary">Create</button>
                </form>
            </div>
        </div>
    </div>


    );

}

export default SaleForm;
