import { useEffect, useState } from 'react';

const TechniciansList = () => {
  const [technicians, setTechnicians] = useState([]);

  const fetchData = async () => {
    const url = 'http://localhost:8080/api/technicians/';
    const response = await fetch(url);

    if (response.ok) {
      const data = await response.json();
      console.log(data);
      setTechnicians(data.list_technician);
      console.log(data.technicians)
    } else {
      console.log('Response not OK;', response.status, response.statusText);
    }
  }



  useEffect(()=>{
    fetchData();
  }, [])

  return (
      <table className="table table-striped">
      <thead>
        <tr>
          <th colSpan="2" style={{ fontSize: '35px'}}>Technicians</th>
        </tr>
        <tr>
          <th>Employee ID</th>
          <th>First Name</th>
          <th>Last Name</th>
        </tr>
      </thead>
      <tbody>
        {technicians.map(technician => {
          return (
              <tr key={technician.employee_id}>
                <td>{technician.employee_id}</td>
                <td>{technician.first_name}</td>
                <td>{technician.last_name}</td>
              </tr>
          );
        })}
      </tbody>
      </table>
  );
}

export default TechniciansList;
