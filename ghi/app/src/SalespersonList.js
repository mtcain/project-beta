import { useEffect, useState } from 'react';

const SalespersonList = () => {
  const [salesperson, setSalesperson] = useState([]);

  const fetchData = async () => {
    const url = 'http://localhost:8090/api/salespeople/';
    const response = await fetch(url);

    if (response.ok) {
      const data = await response.json();
      console.log(data);
      setSalesperson(data.salesperson);
    } else {
      console.log('Response not OK;', response.status, response.statusText);
    }
  }



  useEffect(()=>{
    fetchData();
  }, [])

  return (
      <table className="table table-striped">
      <thead>
        <tr>
          <th colSpan="2" style={{ fontSize: '35px'}}>Salespersons</th>
        </tr>
        <tr>
          <th>First Name</th>
          <th>Last Name</th>
          <th>Employee ID</th>
        </tr>
      </thead>
      <tbody>
        {salesperson.map(salesperson => {
          return (
              <tr key={salesperson.employee_id}>
                <td>{salesperson.first_name}</td>
                <td>{salesperson.last_name}</td>
                <td>{salesperson.employee_id}</td>
              </tr>
          );
        })}
      </tbody>
      </table>
  );
}

export default SalespersonList;
