from django.db import models


class Technician(models.Model):
    first_name = models.CharField(max_length=200)
    last_name = models.CharField(max_length=200)
    employee_id = models.CharField(max_length=200, unique=True)


class AutomobileVO(models.Model):
    import_vin = models.CharField(max_length=17)
    sold = models.BooleanField(default=False)


class Appointment(models.Model):

    STATUS_CHOICES = (
        ("CREATED", "CREATED"),
        ("finished", "finished"),
        ("canceled", "canceled")
    )

    date_time = models.DateTimeField()
    reason = models.CharField(max_length=200)
    vin = models.CharField(max_length=17, unique=True)
    customer = models.CharField(max_length=200)

    status = models.CharField(choices=STATUS_CHOICES, max_length=10, default="CREATED")
    technician = models.ForeignKey(
        Technician,
        related_name="technician",
        on_delete=models.CASCADE
    )

    def finish(self):
        self.status = "finished"
        self.save()

    def cancel(self):
        self.status = "canceled"
        self.save()

    def __str__(self):
        return self.customer

# Create your models here.
