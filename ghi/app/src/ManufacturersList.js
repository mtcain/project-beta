import { useEffect, useState } from 'react';

const ManufacturersList = () => {
  const [manufacturers, setManufacturers] = useState([]);

  const fetchData = async () => {
    const url = 'http://localhost:8100/api/manufacturers/';
    const response = await fetch(url);

    if (response.ok) {
      const data = await response.json();
      setManufacturers(data.manufacturers);
    }
  }

  useEffect(()=>{
    fetchData();
  }, [])

  return (
      <table className="table table-striped">
      <thead>
        <tr>
          <th colSpan="2" style={{ fontSize: '35px'}}>Manufacturers</th>
        </tr>
        <tr>
          <th>Name</th>
        </tr>
      </thead>
      <tbody>
        {manufacturers.map(manufacturer => {
          return (
              <tr key={manufacturer.href}>
                <td>{manufacturer.name}</td>
              </tr>
          );
        })}
      </tbody>
      </table>
  );
}

export default ManufacturersList;
