from django.urls import path

from .views import api_list_technicians, api_list_appointments,api_delete_technicians, api_appointments_delete, api_finish_appointment, api_cancel_appointment, api_list_AutomobileVO

urlpatterns = [
    path("technicians/", api_list_technicians, name="api_create_technician"),
    path("appointments/", api_list_appointments, name="api_create_appointments"),
    path("technicians/<str:employee_id>/", api_delete_technicians, name="api_delete_technicians"),
    path("appointments/<str:vin>/", api_appointments_delete, name="api_appointments_delete"),
    path("appointments/<str:vin>/cancel", api_cancel_appointment, name="api_cancel_appointment"),
    path("appointments/<str:vin>/finish", api_finish_appointment, name="api_finish_appointment"),
    path("automobilevo/", api_list_AutomobileVO, name="api_list_automobilevo")

]
