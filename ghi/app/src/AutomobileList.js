import { useEffect, useState } from 'react';

const AutomobilesList = () => {
  const [automobiles, setAutomobiles] = useState([]);

  const fetchData = async () => {
    const url = 'http://localhost:8100/api/automobiles/';
    const response = await fetch(url);

    if (response.ok) {
      const data = await response.json();
      console.log(data);
      setAutomobiles(data.autos);
    } else {
      console.log('Response not OK;', response.status, response.statusText);
    }
  }



  useEffect(()=>{
    fetchData();
  }, [])

  return (
      <table className="table table-striped">
      <thead>
        <tr>
          <th colSpan="2" style={{ fontSize: '35px'}}>Automobiles</th>
        </tr>
        <tr>
          <th>VIN</th>
          <th>Color</th>
          <th>Year</th>
          <th>Model</th>
          <th>Manufacturer</th>
          <th>Sold</th>
        </tr>
      </thead>
      <tbody>
        {automobiles.map(automobile => {
          return (
              <tr key={automobile.vin}>
                <td>{automobile.vin}</td>
                <td>{automobile.color}</td>
                <td>{automobile.year}</td>
                <td>{automobile.model.name}</td>
                <td>{automobile.model.manufacturer.name}</td>
                <td>{automobile.sold ? 'Yes' : 'No'}</td>
              </tr>
          );
        })}
      </tbody>
      </table>
  );
}

export default AutomobilesList;
