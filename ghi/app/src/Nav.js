import { NavLink } from 'react-router-dom';

function Nav() {
  return (
    <nav className="navbar navbar-expand-lg navbar-dark bg-success">
      <div className="container-fluid">
        <NavLink className="navbar-brand" to="/">CarCar</NavLink>
        <button
          className="navbar-toggler"
          type="button"
          data-bs-toggle="collapse"
          data-bs-target="#navbarSupportedContent"
          aria-controls="navbarSupportedContent"
          aria-expanded="false"
          aria-label="Toggle navigation"
        >
          <span className="navbar-toggler-icon"></span>
        </button>
          <ul className="navbar-nav ml-auto flex-wrap">
            <li className="nav-item">
                  <NavLink className="nav-link" aria-current="page" to="/">Home</NavLink>
                </li>
                <li className="nav-item">
                  <NavLink className="nav-link" aria-current="page" to="/manufacturers">
                    ListManufacturers
                  </NavLink>
                </li>
                <li className="nav-item">
                  <NavLink className="nav-link" aria-current="page" to="/manufacturers/create">
                    CreateManufacturer
                  </NavLink>
                </li>
                <li className="nav-item">
                  <NavLink className="nav-link" aria-current="page" to="/automobiles">
                    AutomobileList
                  </NavLink>
                </li>
                <li className="nav-item">
                  <NavLink className="nav-link" aria-current="page" to="/automobiles/create">
                    AutomobileCreate
                  </NavLink>
                </li>
                <li className="nav-item">
                  <NavLink className="nav-link" aria-current="page" to="/customer/create">
                    CustomerCreate
                  </NavLink>
                </li>
                <li className="nav-item">
                  <NavLink className="nav-link" aria-current="page" to="/customers">
                    CustomerList
                  </NavLink>
                </li>
                <li className="nav-item">
                  <NavLink className="nav-link" aria-current="page" to="/salesperson">
                    SalespersonList
                  </NavLink>
                </li>
                <li className="nav-item">
                  <NavLink className="nav-link" aria-current="page" to="/salesperson/create">
                    SalespersonCreate
                  </NavLink>
                </li>
                <li className="nav-item">
                  <NavLink className="nav-link" aria-current="page" to="/sales">
                    SalesList
                  </NavLink>
                </li>
                <li className="nav-item">
                  <NavLink className="nav-link" aria-current="page" to="/sales/create">
                    SalesCreate
                  </NavLink>
                </li>
                <li className="nav-item">
                  <NavLink className="nav-link" aria-current="page" to="/salespersonhistory">
                    SalespersonHistory
                  </NavLink>
                </li>
                <li className="nav-item">
                      <a className="nav-link" aria-current="page" href="/models">Car Models</a>
                </li>
                <li className="nav-item">
                      <a className="nav-link" aria-current="page" href="/models/create">Create Car Model</a>
                </li>
                <li className="nav-item">
                      <a className="nav-link" aria-current="page" href="/automobiles/create">Create Car</a>
                </li>
                <li className="nav-item">
                      <a className="nav-link" aria-current="page" href="/technicians">Technicians</a>
                </li>
                <li className="nav-item">
                      <a className="nav-link" aria-current="page" href="/technicians/create">Create Technician</a>
                </li>
                <li className="nav-item">
                      <a className="nav-link" aria-current="page" href="/appointments">Appointments List</a>
                </li>
                <li className="nav-item">
                      <a className="nav-link" aria-current="page" href="/appointments/history">Appointment History</a>
                </li>
                <li className="nav-item">
                      <a className="nav-link" aria-current="page" href="/appointment/create">Create an Appointment</a>
                </li>


          </ul>
        </div>
    </nav>
  )
}

export default Nav;
