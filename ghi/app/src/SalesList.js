import { useEffect, useState } from 'react';

const SalesList = () => {
  const [sales, setSales] = useState([]);

  const fetchData = async () => {
    const url = 'http://localhost:8090/api/sales/';
    const response = await fetch(url);

    if (response.ok) {
      const data = await response.json();
      console.log(data);
      setSales(data.sale);
    } else {
      console.log('Response not OK;', response.status, response.statusText);
    }
  }



  useEffect(()=>{
    fetchData();
  }, [])

  return (
      <table className="table table-striped">
      <thead>
        <tr>
          <th colSpan="2" style={{ fontSize: '35px'}}>Sales</th>
        </tr>
        <tr>
          <th>Salesperson Employee ID</th>
          <th>Salesperson Name</th>
          <th>Customer</th>
          <th>Vin</th>
          <th>Price</th>
        </tr>
      </thead>
      <tbody>
        {sales.map(sale => {
          return (
              <tr key={sale.pk}>
                <td>{sale.salesperson.employee_id}</td>
                <td>{sale.salesperson.first_name} {sale.salesperson.last_name}</td>
                <td>{sale.customer.first_name} {sale.customer.last_name}</td>
                <td>{sale.automobile.import_vin}</td>
                <td>{sale.price}</td>
              </tr>
          );
        })}
      </tbody>
      </table>
  );
}

export default SalesList;
